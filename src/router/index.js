import { createRouter, createWebHistory } from 'vue-router';
import DashboardLayout from '@/layout/DashboardLayout';
import UserProfile from '@/views/UserProfile';
import OwnDashboardLayout from '@/layout/Own/OwnDashboardLayout';
import AuthLayout from '@/layout/AuthLayout';
import Websockets from '@/views/websockets/Websockets';
import ProducerDashboard from '@/views/Dashboards/ProducerDashboard';
import ConsumerDashboard from '@/views/Dashboards/ConsumerDashboard';
import NetbeheerderDashboard from '@/views/Dashboards/NetbeheerderDashboard';
import EnergyMarket from '@/views/market/EnergyMarket.vue';
import Prediction from '@/views/prediction/Prediction.vue'
import ProducersManagement from '@/views/ProducersManagement';
var webtoken = require('jsonwebtoken');

var Login = () => import('@/views/Auth/Login.vue');
var Register = () => import('@/views/Auth/Register.vue');

const routes = [
  {
    path: '/producers',
    redirect: '/producers',
    component: DashboardLayout,
    children: [
      {
        path: '/producers',
        name: 'producers',
        components: { default: ProducersManagement },
      },
    ],
  },
  {
    path: '/websockets',
    redirect: '/websockets',
    component: DashboardLayout,
    children: [
      {
        path: '/websockets',
        name: 'websockets',
        components: { default: Websockets },
      },
    ],
  },
  {
    path: '/netbeheerder',
    redirect: '/netbeheerder',
    component: DashboardLayout,
    children: [
      {
        path: '/netbeheerder',
        name: 'netbeheerder',
        components: { default: NetbeheerderDashboard },
      },
      {
        path: '/energy_market',
        name: 'EnergyMarket',
        components: { default: EnergyMarket },
        props: (route) => ({ page: route.query.page })
      },
    ],
  },
  {
	path: '/predict',
    redirect: '/predict',
    component: DashboardLayout,
    children: [
      {
        path: '/predict',
        name: 'predict',
        components: { default: Prediction },
      },
    ]
  },
  {
    path: '/template',
    redirect: '/template/dashboard',
    component: DashboardLayout,
    children: [
      {
        path: '/',
        name: 'dashboard',
        components: { default: ConsumerDashboard },
      },
      {
        path: '/consumer-dashboard',
        name: 'consumer-dashboard',
        components: { default: ConsumerDashboard },
        beforeEnter: (to, from, next) => {
          if (getTokenRole() !== 'Basic_User') next({ path: '/login' });
          else next();
        },
      },
      {
        path: '/profile',
        name: 'profile',
        components: { default: UserProfile },
      },
      {
        path: '/producer-dashboard',
        name: 'ProducerDashboard',
        components: { default: ProducerDashboard },
        beforeEnter: (to, from, next) => {
          if (getTokenRole() !== 'Power4All_User') next({ path: '/login' });
          else next();
        },
      },
    ],
  },
  {
    path: '/',
    redirect: 'login',
    component: AuthLayout,
    children: [
      {
        path: '/login',
        name: 'login',
        components: { default: Login },
      },
      {
        path: '/register',
        name: 'register',
        components: { default: Register },
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  mode: 'history',
  historyApiFallback: true,
  linkActiveClass: 'active',
  routes,
});

function getTokenRole() {
  return webtoken.decode(localStorage.jwt_token).role;
}

export default router;
