import axios from 'axios';

const baseURL = process.env.VUE_APP_BASE_URL;

export const axiosAPI = axios.create({
  baseURL: baseURL,

  withCredentials: false,
  headers: { Accept: 'application/json' },
});

axiosAPI.interceptors.request.use((config) => {
  let token = localStorage.getItem('jwt_token');
  if (token !== null) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});
