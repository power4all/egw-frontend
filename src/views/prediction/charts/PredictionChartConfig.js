const PredictionChartConfig = {

    config(predictionValues) {
      return {
        datasets: [
          {
            label: "Voorspelling",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(252,3,3,0.39)",
            borderColor: "rgb(252,3,3)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "black",
            pointBackgroundColor: "white",
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointHoverBackgroundColor: "yellow",
            pointHoverBorderColor: "brown",
            pointHoverBorderWidth: 2,
            pointRadius: 4,
            pointHitRadius: 10,
            // notice the gap in the data and the spanGaps: false
            data: predictionValues,
            spanGaps: false
          },
        ]
      };
    },
  
    options() {
      return {
        scales: {
          x: {
            type: "time"
          },
          yAxes: [{
            ticks: {
              beginAtZero: true
            },
            scaleLabel: {
              display: true,
              labelString: "Moola",
              fontSize: 20
            }
          }]
        },
        animation: false,
        responsive: true,
        maintainAspectRatio: false
      };
    }
  };
  
  export default PredictionChartConfig;
  