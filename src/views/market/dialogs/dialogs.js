import axios from 'axios';

export function buyOfferDialog(offer, onBought) {
  return {
    show: true,
    header: `Buying offer from ${offer.seller}`,
    inputs: [
      {
        type: "number",
        info: "Insert an amount",
        show: true,
        min: 1,
        max: offer.amountTotal,
        selected: offer.amount,
        callback: (amount) =>
          `Buying ${amount} kW for $${(offer.price * amount).toFixed(2)}`,
      },
    ],
    buttons: [
      {
        title: "Buy",
        callback: (amount) => {
          let request = {
            offerId: offer.id,
            amount: Number(amount),
          };
          const baseURL = process.env.VUE_APP_BASE_URL;
          axios.post(baseURL + "/api/buy", request)
            .then((response) => onBought(response))
            .catch((error) => onBought(error.response));
        },
      },
      {
        title: "Cancel",
        isClose: true,
      },
    ],
  };
}

export function sellOfferDialog(offer, onSold) {
  let amount = offer.offer.amount;
  return {
    show: true,
    header: `Selling offer to ${offer.buyerName}`,
    body: `Sell ${amount} kW for $${(offer.offer.price * amount).toFixed(2)}?`,
    inputs: [{ show: false, }],
    buttons: [
      {
        title: "Sell",
        callback: () => {
          let request = {
            offerId: offer.offer.id,
            amount: Number(amount),
          };
          const baseURL = process.env.VUE_APP_BASE_URL;
          axios.post(baseURL + "/api/sell/offer", request)
            .then((response) => onSold(response))
            .catch((error) => onSold(error.response));
        },
      },
      {
        title: "Cancel",
        isClose: true,
      },
    ],
  };
}

export function offerResponseDialog(header, body) {
  return {
    show: true,
    header: header,
    body: body,
    buttons: [
      {
        title: "Ok",
        isClose: true,
      },
    ],
  };
}