import ConsumerChartData from '@/assets/consumer-chart-data.json';

const ConsumerChartConfig = {

  config(x_time, y_data1, y_data2) {
    return {
      type: "bar",  // <-- define overall chart type
      data: {
        labels: x_time,
        datasets: [{
          type: "line",
          label: "Temperatuur",
          fill: false,
          backgroundColor: "#f5365c",
          borderColor: "#f5365c",
          data: y_data1,
          yAxisID: "left-axis",
          pointRadius: 0
        }, {
          label: "Verbruik",
          fill: true,
          backgroundColor: "rgb(54, 162, 235)",
          borderColor: "rgb(54, 162, 235)",
          data: y_data2,
          yAxisID: "right-axis"
        }]
      },
      options: {
        legend: {
          position: "bottom",
          usePointStyle: true,
          labels: {
            fontColor: "#FFF",
            fontSize: 14
          }
        },
        maintainAspectRatio: false,
        responsive: true,
        title: { display: false },
        tooltips: { mode: "index", intersect: false },
        hover: { mode: "nearest", intersect: true },
        scales: {
          xAxes: [{
            display: true,
            stacked: true,
            scaleLabel: { fontColor: "#FFFFFF", display: false, labelString: "time" },
            ticks: {
              fontColor: "#FFF",
              fontSize: 14
            }
          }],
          yAxes: [{
            type: "linear",
            id: "left-axis",
            display: true,
            position: "left",
            scaleLabel: { fontColor: "#FFFFFF", display: true, labelString: "Temp (°C)" },
            ticks: {
              fontColor: "#FFF", // To format the ticks, coming on the axis/lables which we are passing.
              fontSize: 14
            }
          }, {
            type: "linear",
            id: "right-axis",
            display: true,
            position: "right",
            stacked: false,
            scaleLabel: { fontColor: "#FFFFFF", display: true, labelString: "kWh" },
            gridLines: { drawOnChartArea: false },
            ticks: {
              fontColor: "#FFF", // To format the ticks, coming on the axis/lables which we are passing.
              fontSize: 14
            }
          }]
        }
      }
    };
  },

  getTimeData()  {
    const data = ConsumerChartData.aData;

    let result = data.map((elem) => {
      return elem.dateFrom.slice(10).slice(0, 6);
    });

    return result;
  },
  getConsumedEnergy() {
    const data = ConsumerChartData.aData;

    let result = data.map((elem) => {
      if (elem.usage.r181 > 0) {
        return elem.usage.r181;
      }
      return elem.usage.r182;
    });

    return result;
  },
  getTemperature() {
    const data = ConsumerChartData.aData;
    let prev = 0;

    let result = data.map((elem) => {
      if (elem.temperature) {
        prev = elem.temperature;
        return elem.temperature;
      }
      return prev;
    });

    return result;
  }
};

export default ConsumerChartConfig;
