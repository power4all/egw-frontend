export default {
  config(nuclearValues, solarValues, windValues) {
    return {
      datasets: [
        {
          label: "Nuclear",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(233,74,74,0.36)",
          borderColor: "rgb(233,74,74)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: false
          data: nuclearValues,
          spanGaps: false
        },
        {
          label: "Solar",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(250,166,82,0.36)",
          borderColor: "rgb(250,166,82)",
          borderCapStyle: "square",
          borderDash: [], // try [5, 15] for instance
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: true
          data: solarValues,
          spanGaps: true
        }, {
          label: "Wind",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(100,191,168,0.36)",
          borderColor: "rgb(100,191,168)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: false
          data: windValues,
          spanGaps: false
        }
      ]
    };
  },

  options() {
    return {
      scales: {
        x: {
          type: "time"
        },
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: "Moola",
            fontSize: 20
          }
        }]
      },
      animation: false,
      responsive: true,
      maintainAspectRatio: false
    };
  }
};