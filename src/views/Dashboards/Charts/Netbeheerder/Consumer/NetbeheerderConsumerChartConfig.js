export default {
  config(smallValues, mediumValues, largeValues) {
    return {
      datasets: [
        {
          label: "Small Consumer",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(30,174,152,0.45)",
          borderColor: "rgb(30,174,152)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: false
          data: smallValues,
          spanGaps: false
        },
        {
          label: "Medium Consumer",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(169,241,223,0.35)",
          borderColor: "rgb(169,241,223)",
          borderCapStyle: "square",
          borderDash: [], // try [5, 15] for instance
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: true
          data: mediumValues,
          spanGaps: true
        }, {
          label: "Large Consumer",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(255,255,199,0.37)",
          borderColor: "rgb(255,255,199)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "black",
          pointBackgroundColor: "white",
          pointBorderWidth: 1,
          pointHoverRadius: 8,
          pointHoverBackgroundColor: "yellow",
          pointHoverBorderColor: "brown",
          pointHoverBorderWidth: 2,
          pointRadius: 4,
          pointHitRadius: 10,
          // notice the gap in the data and the spanGaps: false
          data: largeValues,
          spanGaps: false
        }
      ]
    };
  },

  options() {
    return {
      scales: {
        x: {
          type: "time"
        },
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          scaleLabel: {
            display: true,
            labelString: "Moola",
            fontSize: 20
          }
        }]
      },
      animation: false,
      responsive: true,
      maintainAspectRatio: false
    };
  }
};